import { injectReducer } from '../../store/reducers'

export default (store) => ({
  path: 'lockscreen',
  /*  Async getComponent is only invoked when route matches   */
  getComponent (nextState, cb) {
    /*  Webpack - use 'require.ensure' to create a split point
        and embed an async module loader (jsonp) when bundling   */
    require.ensure([], (require) => {
      /*  Webpack - use require callback to define
          dependencies for bundling   */
      const Lockscreen = require('./containers/LockscreenContainer').default
      const reducer = require('./modules/lockscreen').default

      /*  Add the reducer to the store on key 'lockscreen'  */
      injectReducer(store, { key: 'lockscreen', reducer })

      /*  Return getComponent   */
      cb(null, Lockscreen)

    /* Webpack named bundle   */
    }, 'lockscreen')
  }
})
