// ------------------------------------
// Constants
// ------------------------------------
export const CUSTOMER_VISITS_COUNT = 'CUSTOMER_VISITS_COUNT'
export const CUSTOMER_ADD_ITEM = 'CUSTOMER_ADD_ITEM'


// ------------------------------------
// Actions
// ------------------------------------
export function customerVisitIncrement (value = 1) {
  return {
    type: CUSTOMER_VISITS_COUNT,
    payload: value
  }
}

export function customerAddItem (value) {
  return {
    type: CUSTOMER_ADD_ITEM,
    payload: value
  }
}

// ------------------------------------
// Action Handlers
// ------------------------------------
const ACTION_HANDLERS = {
  [CUSTOMER_VISITS_COUNT]: (state, action) => { 
    state.visitsCount = state.visitsCount + action.payload
    return Object.assign({}, state)
  },
  [CUSTOMER_ADD_ITEM]: (state, action) => { 
    const mockedId = Math.floor(Date.now() / 1000)
    const newItem = {
      label: action.payload,
      id: mockedId
    }
    state.dashboardItems.push(newItem)
    return Object.assign({}, state)
  },
 
}

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  visitsCount: 0,
  dashboardItems: [
    {key: 0, label: 'Angular'},
    {key: 1, label: 'JQuery'},
    {key: 2, label: 'Polymer'},
    {key: 3, label: 'ReactJS'}
  ]
}
export default function dashboardReducer (state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}
