import React from 'react'
import { connect } from 'react-redux'
import { LoginVisitIncrement } from '../modules/login'


import Login from 'components/Login'



const mapActionCreators = {
  LoginVisitIncrement: () => LoginVisitIncrement(1),
}

const mapStateToProps = (state) => ({
  login: state.login
})


class LoginContainer extends React.Component {
  componentDidMount() {
    this.props.LoginVisitIncrement();
  }
  render() {
    return (
      <Login {...this.props} />
    );
  }
}



export default connect(mapStateToProps, mapActionCreators)(LoginContainer)
