// ------------------------------------
// Constants
// ------------------------------------
export const LOGIN_VISIT_COUNT = 'LOGIN_VISIT_COUNT'

// ------------------------------------
// Actions
// ------------------------------------
export function LoginVisitIncrement (value = 1) {
  return {
    type: LOGIN_VISIT_COUNT,
    payload: value
  }
}



export const actions = {
  LoginVisitIncrement
}


const ACTION_HANDLERS = {
  [LOGIN_VISIT_COUNT]: (state, action) => {
    state + action.payload
    return Object.assign({},state)
  }
}

const initialState = {
  visitCount:10,
  loginItems:[
    {key:0,label:'pham van A'},
    {key:1,label:'pham van B'},
  ]
}
export default function loginReducer (state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}
