import React from 'react'
import classes from './Login.scss'

export const Login = (props) => {
  const listJSX = props.login.loginItems.map((item,i)=>{
    return <h4 key={i}>item.label</h4>
  })
  return (
    <div className={classes.loginContainer}>
      <h2 >
        FORM CACULATOR:
      </h2>
      <form >
        <input 
          type='input' 
          placeholder='Number 01' 
          style={{width: 100}}
          name='number1'
           />
        <input 
          type='input' 
          placeholder='Number 02' 
          style={{width: 100}}
          name='number2'
           />
        <input 
          type='submit' 
          value={ 'CACULATOR NOW' } />
      </form>
      <p>{props.login.visitCount}</p>
     {listJSX}
    </div>
  )
}
/*Validate dữ liệu*/
Login.propTypes = {
  login: React.PropTypes.object.isRequired,
}

export default Login