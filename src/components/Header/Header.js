import React from 'react'
import { IndexLink, Link } from 'react-router'
import classes from './Header.scss'


import '../../../public/css/main.css';
import { Button,ButtonToolbar,Grid,Row,Col } from '@sketchpixy/rubix';

let loginObj = {
  user: '',
  password: ''
}

const usernameOnChange = (e) => {
  loginObj.user = e.target.value
}

const passwordOnChange = (e) => {
  loginObj.password = e.target.value
}

const prepareLoginJSX = (props) => (<div>
    <form onSubmit={props.handleLogin.bind(undefined, loginObj)}>
      <input 
        type='input' 
        placeholder='username' 
        style={{width: 100}}
        name='username'
        onChange={usernameOnChange} />
      <input 
        type='input' 
        placeholder='password' 
        style={{width: 100}}
        name='password'
        onChange={passwordOnChange} />
      <input 
        type='submit' 
        value={ 'Login Now' } />
    </form>
  </div>)

export const Header = (props) => {
  let loginFormJSX
  let loginMessageJSX = null

  if(props.session.isNotLoggedIn) {
    if(props.session.loginToken === 'invalid') {
      loginMessageJSX = <p>Invalid login details, please try with correct user and password</p>
    }

    loginFormJSX = prepareLoginJSX(props)
  } else {
    loginFormJSX = null
  }

  return (
    
    <div>
      <div className="App">
    <div className="App-header" style={{ height: 170 }}>

      <h2>Welcome to React</h2>
    </div>
    <p className="App-intro">
      To get started, edit <code>src/App.js</code> and save to reload.
    </p>
    <p>
      Adding some Rubix related code:
    </p>
    <div>
     <Grid>
  <Row className="show-grid">
    <Col xs={12} md={8}><code>&lt;{'Col xs={12} md={8}'} /&gt;</code></Col>
    <Col xs={6} md={4}><code>&lt;{'Col xs={6} md={4}'} /&gt;</code></Col>
  </Row>

  <Row className="show-grid">
    <Col xs={6} md={4}><code>&lt;{'Col xs={6} md={4}'} /&gt;</code></Col>
    <Col xs={6} md={4}><code>&lt;{'Col xs={6} md={4}'} /&gt;</code></Col>
    <Col xsHidden md={4}><code>&lt;{'Col xsHidden md={4}'} /&gt;</code></Col>
  </Row>

  <Row className="show-grid">
    <Col xs={6} xsOffset={6}><code>&lt;{'Col xs={6} xsOffset={6}'} /&gt;</code></Col>
  </Row>

  <Row className="show-grid">
    <Col md={6} mdPush={6}><code>&lt;{'Col md={6} mdPush={6}'} /&gt;</code></Col>
    <Col md={6} mdPull={6}><code>&lt;{'Col md={6} mdPull={6}'} /&gt;</code></Col>
  </Row>
  </Grid>



     <ButtonToolbar>
  <Button>Default</Button>
  <Button bsStyle="primary">Primary</Button>
  <Button bsStyle="success">Success</Button>
  <Button bsStyle="info">Info</Button>
  <Button bsStyle="warning">Warning</Button>
  <Button bsStyle="danger">Danger</Button>
  <Button bsStyle="link">Link</Button>
  <Button bsStyle="orange">Orange Button</Button>
  </ButtonToolbar>



      <div><Button bsStyle='green'>Green Button!</Button></div>
      <div><Button bsStyle='red'>Red Button!</Button></div>
      <div><Button bsStyle='blue' outlined>Blue Button!</Button></div>
    </div>
    </div>
      <h1>React Redux Starter Kit</h1>
      <div>
        <IndexLink to='/' activeClassName={classes.activeRoute}>
          Home
        </IndexLink>
        {' · '}
        <Link to='/counter' activeClassName={classes.activeRoute}>
          Counter
        </Link>
        {' · '}
        <Link to='/customer' activeClassName={classes.activeRoute}>
          Customer
        </Link>
        {' · '}
        <Link to='/lockscreen' activeClassName={classes.activeRoute}>
          Lock screen
        </Link>
        {' · '}
        <Link to='/login' activeClassName={classes.activeRoute}>
          login
        </Link>
        {' · '}
        <Link to='/dashboard' activeClassName={classes.activeRoute}>
          Dashboard
        </Link>
        {' · '}
        <Link to='/zen' activeClassName={classes.activeRoute}>
          ZEN
        </Link>
      </div>
      {loginFormJSX}
      {loginMessageJSX}
    </div>
  )
}

export default Header
